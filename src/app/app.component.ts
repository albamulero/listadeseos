import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'wishlist';

  /* Creamos la vaiable time que es un observador, estamos creando un reloj que se ve en pagina Web. Lo que necesita el estar pendiente cuando ser realiza un next, que se realiza cada 1 segundo. Este observable es relametn un observable del String */

  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()),1000);
  });

}
